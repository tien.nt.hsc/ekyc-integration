const { createHttpClient, ClientSettings, UploadImage } = require('@tsocial/trustvision-sdk');
const fs = require('fs');

const httpClient = createHttpClient(
  // process.env.TRUST_ACCESS_KEY, // access key
  '88c6ffa6-c8f6-468f-a2d8-b849c6d7d51d',
  // process.env.TRUST_SECRET_KEY, // secret key
  'uk4fpFxzvq2dHmLw11G0B1lCDHBFARdt',
  // process.env.TRUST_ENV, // environment (testing, staging, production) or API URL
  'staging'
);

const clientSettingsAPI = new ClientSettings(httpClient);
const uploadImageAPI = new UploadImage(httpClient);

clientSettingsAPI
  .getClientSettings()
  .then((result) => {
    // result contains information about card types and other settings
    console.log(result);
  })
  .catch((error) => {
    console.log(error);
  });

uploadImageAPI.uploadImage({
    file: fs.createReadStream("./IMG_0736.jpg"), // file object
    imageLabel: 'portrait', // image label
  }).then((result) => {
    // result contains information about card types and other settings
    console.log(result);
  })
  .catch((error) => {
    console.log(error.message);
  });
